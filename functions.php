<?php
/**
 * Setup Child Theme Styles
 */
function discount_grocery_warehouse_enqueue_styles() {
	wp_enqueue_style( 'discount_grocery_warehouse-style', get_stylesheet_directory_uri() . '/style.css', false, '1.0' );
}
add_action( 'wp_enqueue_scripts', 'discount_grocery_warehouse_enqueue_styles', 20 );


/**
 * Setup Child Theme Palettes
 *
 * @param string $palettes registered palette json.
 * @return string
 */
function discount_grocery_warehouse_change_palette_defaults( $palettes ) {
	$palettes = '{"palette":[{"color":"#2B6CB0","slug":"palette1","name":"Palette Color 1"},{"color":"#215387","slug":"palette2","name":"Palette Color 2"},{"color":"#1A202C","slug":"palette3","name":"Palette Color 3"},{"color":"#2D3748","slug":"palette4","name":"Palette Color 4"},{"color":"#4A5568","slug":"palette5","name":"Palette Color 5"},{"color":"#718096","slug":"palette6","name":"Palette Color 6"},{"color":"#EDF2F7","slug":"palette7","name":"Palette Color 7"},{"color":"#F7FAFC","slug":"palette8","name":"Palette Color 8"},{"color":"#ffffff","slug":"palette9","name":"Palette Color 9"}],"second-palette":[{"color":"#2B6CB0","slug":"palette1","name":"Palette Color 1"},{"color":"#215387","slug":"palette2","name":"Palette Color 2"},{"color":"#1A202C","slug":"palette3","name":"Palette Color 3"},{"color":"#2D3748","slug":"palette4","name":"Palette Color 4"},{"color":"#4A5568","slug":"palette5","name":"Palette Color 5"},{"color":"#718096","slug":"palette6","name":"Palette Color 6"},{"color":"#EDF2F7","slug":"palette7","name":"Palette Color 7"},{"color":"#F7FAFC","slug":"palette8","name":"Palette Color 8"},{"color":"#ffffff","slug":"palette9","name":"Palette Color 9"}],"third-palette":[{"color":"#2B6CB0","slug":"palette1","name":"Palette Color 1"},{"color":"#215387","slug":"palette2","name":"Palette Color 2"},{"color":"#1A202C","slug":"palette3","name":"Palette Color 3"},{"color":"#2D3748","slug":"palette4","name":"Palette Color 4"},{"color":"#4A5568","slug":"palette5","name":"Palette Color 5"},{"color":"#718096","slug":"palette6","name":"Palette Color 6"},{"color":"#EDF2F7","slug":"palette7","name":"Palette Color 7"},{"color":"#F7FAFC","slug":"palette8","name":"Palette Color 8"},{"color":"#ffffff","slug":"palette9","name":"Palette Color 9"}],"active":"palette"}';
	return $palettes;
}
add_filter( 'kadence_global_palette_defaults', 'discount_grocery_warehouse_change_palette_defaults', 20 );

/**
 * Setup Child Theme Defaults
 *
 * @param array $defaults registered option defaults with kadence theme.
 * @return array
 */
function discount_grocery_warehouse_change_option_defaults( $defaults ) {
	$new_defaults = '{"custom_logo":19,"header_desktop_items":{"top":{"top_left":[],"top_left_center":[],"top_center":[],"top_right_center":[],"top_right":[]},"main":{"main_left":[],"main_left_center":[],"main_center":["search-bar"],"main_right_center":[],"main_right":["social"]},"bottom":{"bottom_left":["logo"],"bottom_left_center":[],"bottom_center":["navigation"],"bottom_right_center":[],"bottom_right":[]},"flag":false},"header_social_items":{"items":[{"id":"facebook","enabled":true,"source":"image","url":"http:\/\/discount-grocery-warehouse.local\/wp-content\/uploads\/2024\/02\/Frame-171.png","imageid":27,"width":24,"icon":"facebook","label":"Facebook"},{"id":"email","enabled":true,"source":"image","url":"http:\/\/discount-grocery-warehouse.local\/wp-content\/uploads\/2024\/02\/Frame-172.png","imageid":28,"width":24,"icon":"email","label":"Email","svg":""},{"id":"custom1","enabled":true,"source":"image","url":"http:\/\/discount-grocery-warehouse.local\/wp-content\/uploads\/2024\/02\/Frame-173.png","imageid":29,"width":24,"icon":"custom1","label":"Phone","svg":""}],"flag":true},"header_search_bar_width":{"size":552,"unit":"px"},"header_top_padding":{"size":{"desktop":["15","","",""]},"unit":{"desktop":"px"},"locked":{"desktop":false},"flag":false},"logo_layout":{"include":{"mobile":"","tablet":"","desktop":"logo_only"},"layout":{"mobile":"","tablet":"","desktop":"standard"},"flag":true},"header_logo_padding":{"size":{"desktop":["","","",""]},"unit":{"desktop":"px"},"locked":{"desktop":false},"flag":false},"primary_navigation_stretch":true,"primary_navigation_fill_stretch":false,"content_width":{"size":1512,"unit":"px"},"content_narrow_width":{"size":1340,"unit":"px"},"content_spacing":{"size":{"mobile":2,"tablet":3,"desktop":0},"unit":{"mobile":"rem","tablet":"rem","desktop":"rem"},"flag":true},"content_edge_spacing":{"size":{"mobile":"","tablet":"","desktop":0},"unit":{"mobile":"rem","tablet":"rem","desktop":"rem"},"flag":true},"boxed_spacing":{"size":{"mobile":1.5,"tablet":2,"desktop":0},"unit":{"mobile":"rem","tablet":"rem","desktop":"rem"},"flag":true},"base_font":{"size":{"desktop":17},"lineHeight":{"desktop":1.6000000000000001},"family":"Archivo","google":true,"weight":"normal","variant":"regular","color":"#383838","sizeType":"px","lineType":"-","letterSpacing":{"desktop":""},"spacingType":"em","style":"normal","transform":"","fallback":"sans-serif","flag":false},"h1_font":{"size":{"desktop":32},"lineHeight":{"desktop":1.5},"family":"inherit","google":false,"weight":"700","variant":"700","color":"#383838","sizeType":"px","lineType":"-","letterSpacing":{"desktop":""},"spacingType":"em","style":"normal","transform":"","fallback":"","flag":true},"h2_font":{"size":{"desktop":28},"lineHeight":{"desktop":1.5},"family":"inherit","google":false,"weight":"700","variant":"700","color":"#383838","sizeType":"px","lineType":"-","letterSpacing":{"desktop":""},"spacingType":"em","style":"normal","transform":"","fallback":"","flag":true},"h3_font":{"size":{"desktop":24},"lineHeight":{"desktop":1.5},"family":"inherit","google":false,"weight":"700","variant":"700","color":"#383838","sizeType":"px","lineType":"-","letterSpacing":{"desktop":""},"spacingType":"em","style":"normal","transform":"","fallback":"","flag":true},"h4_font":{"size":{"desktop":22},"lineHeight":{"desktop":1.5},"family":"inherit","google":false,"weight":"700","variant":"700","color":"#383838","sizeType":"px","lineType":"-","letterSpacing":{"desktop":""},"spacingType":"em","style":"normal","transform":"","fallback":"","flag":true},"h5_font":{"size":{"desktop":20},"lineHeight":{"desktop":1.5},"family":"inherit","google":false,"weight":"700","variant":"700","color":"#383838","sizeType":"px","lineType":"-","letterSpacing":{"desktop":""},"spacingType":"em","style":"normal","transform":"","fallback":"","flag":true},"h6_font":{"size":{"desktop":18},"lineHeight":{"desktop":1.5},"family":"inherit","google":false,"weight":"700","variant":"700","color":"#383838","sizeType":"px","lineType":"-","letterSpacing":{"desktop":""},"spacingType":"em","style":"normal","transform":"","fallback":"","flag":true},"post_title_layout":"above","post_title_inner_layout":"fullwidth","post_tags":false,"post_navigation":false,"post_related":false,"post_related_carousel_loop":false,"post_comments":false,"post_comments_date":false,"post_layout":"fullwidth","post_content_style":"boxed","page_layout":"fullwidth","post_archive_title_inner_layout":"fullwidth"}';
	$new_defaults = json_decode( $new_defaults, true );
	return wp_parse_args( $new_defaults, $defaults );
}
add_filter( 'kadence_theme_options_defaults', 'discount_grocery_warehouse_change_option_defaults', 20 );
/**
 * Setup Child Theme Starter
 *
 * @param array $data registered custom templates.
 * @return array
 */
function discount_grocery_warehouse_child_add_starter_templates( $data ) {
	$data['discount_grocery_warehouse'] = array(
		'slug'                => 'discount_grocery_warehouse',
		'name'                => 'Discount Grocery Warehouse',
		'local_content'       => get_stylesheet_directory() . '/starter/content.xml',
		'local_widget_data'   => get_stylesheet_directory() . '/starter/widget_data.json',
		'local_theme_options' => get_stylesheet_directory() . '/starter/theme_options.json',
		'url'                 => '',
		'image'               => get_stylesheet_directory_uri() . '/starter/preview.png',
		'ecommerce'           => true,
		'homepage'            => 'Home',
		'blogpage'            => '',
		'type'                => 'blocks',
		'plugins'             => array(
			'gravityforms/gravityforms.php','advanced-custom-fields/acf.php','duplicate-post/duplicate-post.php','gravityformsmailchimp/mailchimp.php','gravityformsstripe/stripe.php','kadence-blocks-pro/kadence-blocks-pro.php','kadence-blocks/kadence-blocks.php','kadence-pro/kadence-pro.php','kadence-woo-extras/kadence-woo-extras.php','woocommerce/woocommerce.php',
		),
		'menus'       => array(
			'primary' => array(
				'menu'  => 'primary',
				'title' => 'Primary',
			),

		),
	);
	return $data;
}
add_filter( 'kadence_starter_templates_custom_array', 'discount_grocery_warehouse_child_add_starter_templates', 20 );

add_filter( 'kadence_custom_child_starter_templates_enable', '__return_true' );

/**
 * Setup Child Theme Starter Brand
 *
 * @param string $name the brand name.
 * @return string
 */
function discount_grocery_warehouse_child_add_starter_templates_name( $name ) {
	return 'Pro Design';
}
add_filter( 'kadence_custom_child_starter_templates_name', 'discount_grocery_warehouse_child_add_starter_templates_name', 20 );

/**
 * Setup Child Theme Starter Logo
 *
 * @param string $url the file url.
 * @return string
 */
function discount_grocery_warehouse_child_add_starter_templates_logo( $url ) {
	return get_stylesheet_directory_uri() . '/starter/logo.png';
}
add_filter( 'kadence_custom_child_starter_templates_logo', 'discount_grocery_warehouse_child_add_starter_templates_logo', 20 );
